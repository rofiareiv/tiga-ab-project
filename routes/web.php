<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OkeController;
use App\Http\Controllers\BeritaController;
/**
 * Route itu sama dengan URL / Address / Alamat
 */

// Route::get('/', function () {
//     return view('page.content');
// });
Route::get('/', [BeritaController::class, 'index']);
Route::get('/about', [OkeController::class, 'about']);
